# NFTX-MLBC Deposit

A contract that automates the process of purchasing an MLBC NFT from the old MLBC marketplace and deposits it into the NFTX vault.

## Hardhat

https://hardhat.org/hardhat-network/

Install
```console
npm install --save-dev hardhat
```

Run local
```console
npx hardhat node
```


## Forking Mainnet
```console
npx hardhat node --fork https://eth-mainnet.alchemyapi.io/v2/<YOUR ALCHEMY KEY>

truffle migrate --reset

```

## Tests

```console
truffle test
```