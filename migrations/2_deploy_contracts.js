let MLBCDeposit = artifacts.require("MLBCDeposit")

module.exports = async function (deployer, network) {

    try {

        let mlbcAddress = "0x8c9b261faef3b3c2e64ab5e58e04615f8c788099";
        let mlbcMarketAddress = "0xe7232a9fd8bf427aa41918bc008d32290e22990e";
        let mlbcNftxVaultAddress = "0x7094b71F08f3Ab06b6592be00599C7F47922A51E";

        await deployer.deploy(MLBCDeposit, 
                              mlbcAddress,
                              mlbcMarketAddress, 
                              mlbcNftxVaultAddress,
                            )

    } catch (e) {
        console.log(`Error in migration: ${e.message}`)
    }
}