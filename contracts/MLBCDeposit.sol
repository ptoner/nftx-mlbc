// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./console.sol";
import "./external/MLBCMarket.sol";
import "./external/MLBC.sol";
import "./external/NFTX.sol";



contract MLBCDeposit  {
    
    MLBC public _mlbc;
    MLBCMarket public _mlbcMarket;
    NFTX public _mlbNftxVault; 

    constructor(address mlbcAddress, address _mlbcMarketAddress, address _mlbcNftxVaultAddress) {

        _mlbc = MLBC(mlbcAddress);
        _mlbcMarket = MLBCMarket(_mlbcMarketAddress);
        _mlbNftxVault = NFTX(_mlbcNftxVaultAddress);

        _mlbc.setApprovalForAll(_mlbcNftxVaultAddress, true);

    }

    function _buy(uint256 _tokenId, uint256 currentPrice) private {

        //Buy the NFT
        _mlbcMarket.bid{ value:currentPrice }( _tokenId );

        console.log("Successful purchase");
    }

    function _mint(uint256 _tokenId) private {

        uint256[] memory nftIds = new uint256[](1);
        nftIds[0] = _tokenId;
        
        uint256[] memory amounts = new uint256[](1);
        amounts[0] = 1;

        _mlbNftxVault.mint(nftIds, amounts);
        
        console.log("Minting success");

    }

    function _getCost(uint256 _tokenId) private view returns (uint256) {
        return _mlbcMarket.getCurrentPrice(_tokenId);
    }

    function buyAndMint(uint256 tokenId) public payable {

        console.log("begin buyAndDeposit");

        //Make sure there's no existing balance
        require(_mlbNftxVault.balanceOf(address(this)) == 0, "Non-zero vault balance");

        //Make sure token exists
        require(_mlbc.exists(tokenId) == true, "Token does not exist");

        //Get the cost of the NFT
        uint256 currentPrice = _getCost(tokenId);
        console.log("currentPrice: %s, msg.value: %s", currentPrice, msg.value);

        //Make sure we have the exact amount of ETH
        require(currentPrice > 0, "Not for sale");
        require(currentPrice == msg.value, "Wrong ETH amount");

        //Buy from MLBC contract
        _buy(tokenId, currentPrice);

        //Mint NTFX vault token
        _mint(tokenId);

        //Validate our balance
        uint256 vaultBalance = _mlbNftxVault.balanceOf(address(this));
        require(vaultBalance == 950000000000000000, "Incorrect balance");

        //Transfer balance to msg.sender
        _mlbNftxVault.transfer(msg.sender, vaultBalance);

    }

    receive() external payable { }
}

