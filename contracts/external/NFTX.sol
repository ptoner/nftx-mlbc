// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
    
contract NFTX {

    function mint(uint256[] memory nftIds, uint256[] memory amounts)
        public
        payable
        virtual
    {}

    function balanceOf(address _owner) public returns (uint256) {}

    function transfer(address recipient, uint256 amount) external returns (bool) {}

}


