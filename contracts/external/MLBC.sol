// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
    
contract MLBC {

    function exists(uint256 _tokenId) public view returns (bool _exists) {}
    function ownerOf(uint256 _tokenId) external view returns (address) {}
    function setApprovalForAll(address _operator, bool _approved) public {}
}


