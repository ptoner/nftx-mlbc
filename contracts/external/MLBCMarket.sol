// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
    
contract MLBCMarket {

    /// @dev Modifier to allow actions only when the contract IS NOT paused
    modifier whenNotPaused() {
        _;
    }

    function bid(uint256 _tokenId) public whenNotPaused payable {
    }

    function getCurrentPrice(uint256 _tokenId) external view returns (uint256) {}
    
}


